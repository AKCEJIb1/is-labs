﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InformationSecurity.Labs.Framework
{
    public class BlockEncryptor
    {
        #region Data
        /// <summary>
        /// Таблица преобразований 8 x 8 бит.
        /// </summary>
        private byte[] _sblockTable;
        #endregion

        #region Properties
        /// <summary>
        /// Ключ шифрования.
        /// </summary>
        public uint Key { get; set; }
        /// <summary>
        /// Количество раундов шифрования.
        /// </summary>
        public int NumRounds { get; }
        #endregion

        #region .ctor
        public BlockEncryptor(
            int numRounds,
            uint key)
        {
            NumRounds = numRounds;
            Key = key;

            _sblockTable = new byte[256];
            GenerateSBlockTable();
        }

        /// <summary>
        /// Генерирует таблицу по правилу (i * (i + 1) shr 1)) mod 256.
        /// </summary>
        private void GenerateSBlockTable()
        {
            for (int i = 0; i < 256; i++)
            {
                _sblockTable[i] = (byte)((i * (i + 1) >> 1) % 256);
            }
        }
        #endregion

        #region Encrypt Methods
        /// <summary>
        /// Запускает алгоритм блочного шифрования с помощью сети Фейстеля.
        /// </summary>
        /// <param name="data"><see cref="Tuple"/> содержащая в себе левую и правую ветвь блока данных.</param>
        /// <returns>Две зашифрованые ветви без финальной перестановки.</returns>
        public Tuple<uint, uint> Encrypt(
            Tuple<uint, uint> data)
        {
            return Encrypt(data.Item1, data.Item2);
        }
        /// <summary>
        /// Запускает алгоритм блочного шифрования с помощью сети Фейстеля с передаваемым ключем.
        /// </summary>
        /// <param name="data"><see cref="Tuple"/> содержащая в себе левую и правую ветвь блока данных.</param>
        /// <param name="key">Ключ для шифрования.</param>
        /// <returns>Две зашифрованые ветви без финальной перестановки.</returns>
        public Tuple<uint, uint> Encrypt(
            Tuple<uint, uint> data,
            uint key)
        {
            return Encrypt(data.Item1, data.Item2, key);
        }
        /// <summary>
        /// Запускает алгоритм блочного шифрования с помощью сети Фейстеля.
        /// </summary>
        /// <param name="leftBranch">Левая ветвь блока данных.</param>
        /// <param name="rightBranch">Правая ветвь блока данных.</param>
        /// <returns>Две зашифрованые ветви без финальной перестановки.</returns>
        public Tuple<uint, uint> Encrypt(
            uint leftBranch,
            uint rightBranch)
        {

            for (int i = 0; i < NumRounds; i++)
            {
                var tempBranch = TransformFunction(leftBranch, Key) ^ rightBranch;

                rightBranch = leftBranch;
                leftBranch = tempBranch;
            }

            return new Tuple<uint, uint>(leftBranch, rightBranch);
        }
        /// <summary>
        /// Запускает алгоритм блочного шифрования с помощью сети Фейстеля с передаваемым ключем.
        /// </summary>
        /// <param name="leftBranch">Левая ветвь блока данных.</param>
        /// <param name="rightBranch">Правая ветвь блока данных.</param>
        /// <param name="key">Ключ для шифрования.</param>
        /// <returns>Две зашифрованые ветви без финальной перестановки.</returns>
        public Tuple<uint, uint> Encrypt(
            uint leftBranch,
            uint rightBranch,
            uint key)
        {

            for (int i = 0; i < NumRounds; i++)
            {
                var tempBranch = TransformFunction(leftBranch, key) ^ rightBranch;

                rightBranch = leftBranch;
                leftBranch = tempBranch;
            }

            return new Tuple<uint, uint>(leftBranch, rightBranch);
        }
        #endregion

        #region Decrypt Methods

        /// <summary>
        /// Запускает алгоритм блочного расшифрования с помощью сети Фейстеля.
        /// </summary>
        /// <param name="encryptedBranches">
        /// <see cref="Tuple"/> содержащая в себе зашифрованную левую и правую ветвь блока данных.</param>
        /// <returns>Две расшифрованные ветви без финальной перестановки.</returns>
        public Tuple<uint, uint> Decrypt(
            Tuple<uint, uint> encryptedBranches)
        {
            return Decrypt(encryptedBranches.Item1, encryptedBranches.Item2);
        }

        /// <summary>
        /// Запускает алгоритм блочного расшифрования с помощью сети Фейстеля с передаваемым ключем.
        /// </summary>
        /// <param name="encryptedBranches"><see cref="Tuple"/> содержащая в себе левую и правую ветвь зашифрованного блока данных.</param>
        /// <param name="key">Ключ для расшифрования.</param>
        /// <returns>Две зашифрованые ветви без финальной перестановки.</returns>
        public Tuple<uint, uint> Decrypt(
            Tuple<uint, uint> encryptedBranches,
            uint key)
        {
            return Decrypt(encryptedBranches.Item1, encryptedBranches.Item2, key);
        }

        /// <summary>
        /// Запускает алгоритм блочного расшифрования с помощью сети Фейстеля.
        /// </summary>
        /// <param name="leftBranch">Левая ветвь блока данных.</param>
        /// <param name="rightBranch">Правая ветвь блока данных.</param>
        /// <returns>Две расшифрованные ветви без финальной перестановки.</returns>
        public Tuple<uint, uint> Decrypt(
            uint leftBranch,
            uint rightBranch)
        {

            for (int i = 0; i < NumRounds; i++)
            {
                var tempBranch = TransformFunction(rightBranch, Key) ^ leftBranch;

                leftBranch = rightBranch;
                rightBranch = tempBranch;
            }

            return new Tuple<uint, uint>(leftBranch, rightBranch);
        }

        /// <summary>
        /// Запускает алгоритм блочного шифрования с помощью сети Фейстеля.
        /// </summary>
        /// <param name="leftBranch">Левая ветвь блока данных.</param>
        /// <param name="rightBranch">Правая ветвь блока данных.</param>
        /// <param name="key">Ключ для расшифрования.</param>
        /// <returns>Две зашифрованые ветви без финальной перестановки.</returns>
        public Tuple<uint, uint> Decrypt(
            uint leftBranch,
            uint rightBranch,
            uint key)
        {

            for (int i = 0; i < NumRounds; i++)
            {
                var tempBranch = TransformFunction(rightBranch, key) ^ leftBranch;

                leftBranch = rightBranch;
                rightBranch = tempBranch;
            }

            return new Tuple<uint, uint>(leftBranch, rightBranch);
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Функция преобразования ветви (F - функция).
        /// </summary>
        private uint TransformFunction(
            uint x,
            uint key)
        {
            var inputBlock = TransformBlock(x);
            var blockWithKey = inputBlock + key;
            var shiftedBlock = CyclicShift.ROR(blockWithKey, 3);
            var outputBlock = TransformBlock(shiftedBlock);

            return outputBlock;
        }
        /// <summary>
        /// Метод разбивающий <see cref="uint"/> на 4 <see cref="byte"/>, которые передаются в таблицу преобразований.
        /// </summary>
        private uint TransformBlock(uint x)
        {
            var byte1 = SBlock((byte)(x >> 24));
            var byte2 = SBlock((byte)(x >> 16));
            var byte3 = SBlock((byte)(x >> 8));
            var byte4 = SBlock((byte)(x >> 0));

            uint result = 0;

            result |= (uint)(byte1 << 24);
            result |= (uint)(byte2 << 16);
            result |= (uint)(byte3 << 8);
            result |= (uint)(byte4 << 0);

            return result;
        }
        /// <summary>
        /// Метод который реализует правило выбора значения из таблицы.
        /// </summary>
        private byte SBlock(byte x)
        {
            return _sblockTable[x];
        }

        /// <summary>
        /// Сжимает сообщение до правильного <see cref="uint"/> массива.
        /// Каждые 4 символа преобразуются в 1 <see cref="uint"/>.
        /// </summary>
        /// <param name="message">Сообщение для сжатия.</param>
        /// <returns>Сжатое в массив <see cref="uint"/> сообщение.</returns>
        public static List<uint> CompressStringToUIntArray(string message)
        {
            var blockSize = 8; // bytes
            var halfBlockSize = blockSize / 2; // 4 bytes => uint

            var byteMsg = Encoding.Default
                .GetBytes(message)
                .ToList();


            while (byteMsg.Count % halfBlockSize != 0)
                byteMsg.Add(0);


            var compactedArray = new List<uint>();

            for (int i = 0; i < byteMsg.Count; i += halfBlockSize)
            {
                var compact = BitConverter.ToUInt32(
                    byteMsg.Skip(i).Take(halfBlockSize).Reverse().ToArray(), 0);

                compactedArray.Add(compact);
            }

            if (compactedArray.Count % 2 != 0)
                compactedArray.Add(0);

            return compactedArray;
        }

        /// <summary>
        /// Создает плоский массив <see cref="uint"/> элементов из списка зашифрованных блоков.
        /// </summary>
        /// <param name="encryptedList">Зашифрованые сетью Фейстеля блоки.</param>
        /// <returns>Плоский массив из элементов зашифрованных блоков.</returns>
        public static uint[] CreateUIntArray(List<Tuple<uint, uint>> encryptedList)
        {
            var byteEncryptedMsg = encryptedList
               .SelectMany(x =>
                   new uint[] {
                        x.Item1,
                        x.Item2})
               .ToArray();

            return byteEncryptedMsg;
        }

        /// <summary>
        /// Преобразует расшифрованные сетью Фейстеля блоки в текстовое сообщение.
        /// </summary>
        /// <param name="decryptedList">Список расшифрованных блоков.</param>
        /// <returns>Исходное сообщение.</returns>
        public static string DecryptedListToString(List<Tuple<uint, uint>> decryptedList)
        {
            var byteDecryptedMsg = decryptedList
                .SelectMany(x =>
                    new byte[]
                    {
                        (byte)((x.Item1 & 0xFF000000) >> 24),
                        (byte)((x.Item1 & 0x00FF0000) >> 16),
                        (byte)((x.Item1 & 0x0000FF00) >> 8),
                        (byte)((x.Item1 & 0x000000FF) >> 0),

                        (byte)((x.Item2 & 0xFF000000) >> 24),
                        (byte)((x.Item2 & 0x00FF0000) >> 16),
                        (byte)((x.Item2 & 0x0000FF00) >> 8),
                        (byte)((x.Item2 & 0x000000FF) >> 0),
                    })
                .ToArray();

            return Encoding.Default.GetString(byteDecryptedMsg);
        }
        
        #endregion
    }
}
