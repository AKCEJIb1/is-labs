﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InformationSecurity.Labs.Framework
{
    public class CryptoHasher
    {
        private byte[] _hashSum;

        public CryptoHasher(
            byte[] hashSum = null)
        {
            
            _hashSum = hashSum ?? new byte[24];

            if (_hashSum.Length != 24)
                throw new ArgumentOutOfRangeException(
                    "hashSum",
                    "Hash sum array should be 24 bytes length.");
        }

        private byte GBlock(
            byte upperValue,
            byte leftValue)
        {
            byte result = (byte)(upperValue + 23);
            result = CyclicShift.ROL(result, 3);
            result += leftValue;

            return result;
        }

        private (byte Bottom, byte Right) TBlock(
            byte upperValue,
            byte upperLeftValue,
            byte leftValue)
        {
            upperValue = CyclicShift.ROL(upperValue, 3);
            byte xor1 = (byte)(leftValue ^ upperValue);
            byte xor2 = (byte)(165 ^ upperLeftValue);
            byte add = (byte)(xor1 + xor2);
            return (xor1, add);
        }

        private byte[] RunRow(int index, byte[] array)
        {
            var newArray = new byte[129];
            var gBlockValue = GBlock(array[0], _hashSum[index]);

            newArray[0] = gBlockValue;

            var tBlockValue = TBlock(array[1], array[1 - 1], gBlockValue);
            newArray[1] = tBlockValue.Bottom;

            for(int i = 2; i < 129; i++)
            {
                tBlockValue = TBlock(array[i], array[i-1], tBlockValue.Right);
                newArray[i] = tBlockValue.Bottom;
            }

            _hashSum[index] = tBlockValue.Right;

            return newArray;
        }

        public byte[] GenerateHash(
            ulong[] data)
        {
            var bytesData = data
                .SelectMany(x => 
                    new byte[]
                    { 
                        (byte)((x & 0xFF00000000000000) >> 56),
                        (byte)((x & 0x00FF000000000000) >> 48),
                        (byte)((x & 0x0000FF0000000000) >> 40),
                        (byte)((x & 0x000000FF00000000) >> 32),
                        (byte)((x & 0x00000000FF000000) >> 24),
                        (byte)((x & 0x0000000000FF0000) >> 16),
                        (byte)((x & 0x000000000000FF00) >> 8),
                        (byte)((x & 0x00000000000000FF) >> 0),
                    })
                .ToArray();

            var msg = Encoding.Default
                .GetString(bytesData, 0, bytesData.Length);

            return GenerateHash(msg);
        }

        public byte[] GenerateHash(
            string message)
        {
            message += message.Length;
            var currentMessageLen = message.Length;

            for (int i = 0; i < 128 / currentMessageLen; i++)
            {
                for (int j = 0; j < currentMessageLen; j++)
                {
                    message += message[j];
                }
            }
            var msgBytes = Encoding.Default.GetBytes(
                new string(message.Take(128).ToArray()));


            var initialArray = new byte[129];
            initialArray[0] = 1;
            msgBytes.CopyTo(initialArray, 1);

            var arr = RunRow(0, initialArray);

            for (int i = 1; i < 24; i++)
            {
                arr = RunRow(i, arr);
            }

            return _hashSum;
        }
    }
}
