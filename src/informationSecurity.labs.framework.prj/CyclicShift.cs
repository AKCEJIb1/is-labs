﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InformationSecurity.Labs.Framework
{
    public static class CyclicShift
    {
        public static uint ROR(
           uint value,
           int bits)
        {
            if (bits == 0) return value;

            var right = value >> 1;
            if (bits > 1)
            {
                right &= 0x7FFFFFFF;
                right >>= bits - 1;
            }
            return (value << (32 - bits)) | right;
        }

        public static uint ROL(
            uint value,
            int bits)
        {
            if (bits == 0) return value;

            var right = value >> 1;
            if ((32 - bits) > 1)
            {
                right &= 0x7FFFFFFF;
                right >>= 32 - bits - 1;
            }
            return value << bits | right;
        }

        public static byte ROL(
            byte value,
            int bits)
        {
            if (bits == 0) return value;

            var right = (byte)(value >> 1);
            if ((8 - bits) > 1)
            {
                right &= 0x7F;
                right >>= 8 - bits - 1;
            }
            return (byte)(value << bits | right);
        }

        public static byte ROR(
           byte value,
           int bits)
        {
            if (bits == 0) return value;

            var right = (byte)(value >> 1);
            if (bits <= 8)
            {
                right &= 0x7F;
                right >>= bits - 1;
            }
            else
                throw new ArgumentOutOfRangeException("bits", "Bits count must be 8 or lower.");
            return (byte)((value << (8 - bits)) | right);
        }
    }
}
