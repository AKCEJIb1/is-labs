﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InformationSecurity.Labs.Framework
{
    public class RandomStreamNormalizer
    {

        private byte[] _sBlock;

        public RandomStreamNormalizer()
        {
            InitSBlock();
        }

        private void InitSBlock()
        {
            _sBlock = new byte[16] { 
                5, 11, 6, 8,
                3, 13, 9, 7,
                10, 4, 12, 2,
                15, 1, 0, 14};
        }

        private byte SBlock(byte x)
        {
            return _sBlock[x];
        }

        public byte[] GenerateKey(ulong[] entropyArray)
        {
            var resultArr = new List<byte>();

            foreach(var x in entropyArray)
            {
                var norm = Normalize(x);
                resultArr.Add(norm.High);
                resultArr.Add(norm.Low);
            }

            return resultArr.ToArray();
        }

        private (byte High, byte Low) Normalize(ulong value)
        {
            var bytes = BitConverter.GetBytes(value);

            byte xor1 = 0;
            byte xor2 = 0;
            byte xor3 = 0;
            byte xor4 = 0;

            for (int i = 7; i >= 0; i-=2)
            {

                var bit4_1_h = (byte)((bytes[i] & 0xF0) >> 4);
                var bit4_1_l = (byte)(bytes[i] & 0x0F);

                var bit4_2_h = (byte)((bytes[i - 1] & 0xF0) >> 4);
                var bit4_2_l = (byte)(bytes[i - 1] & 0x0F);

                xor1 = SBlock((byte)(xor1 ^ bit4_1_h));
                xor2 = SBlock((byte)(xor2 ^ bit4_1_l));
                xor3 = SBlock((byte)(xor3 ^ bit4_2_h));
                xor4 = SBlock((byte)(xor4 ^ bit4_2_l));
            }

            byte resultHigh = 0;
            byte resultLow = 0;

            resultHigh |= (byte)(xor1 << 4);
            resultHigh |= (byte)(xor2 << 0);
            resultLow |= (byte)(xor3 << 4);
            resultLow |= (byte)(xor4 << 0);

            return (resultHigh, resultLow);
        }

    }
}
