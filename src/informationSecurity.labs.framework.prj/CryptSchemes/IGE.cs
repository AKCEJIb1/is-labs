﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InformationSecurity.Labs.Framework.CryptSchemes
{
    public class IGE : ICryptScheme
    {
        private BlockEncryptor _be;
        private uint _syncVectorLeft, _syncVectorRight;

        public IGE(
            BlockEncryptor be = null,
            uint? syncVectorLeft = null,
            uint? syncVectorRight = null)
        {
            var rand = new Random();

            _be = be ?? new BlockEncryptor(12, (uint)rand.Next());
            _syncVectorLeft = syncVectorLeft ?? (uint)rand.Next();
            _syncVectorRight = syncVectorRight ?? (uint)rand.Next();
        }

        public uint[] Encrypt(
            string message)
        {
            var compactedArray = BlockEncryptor.CompressStringToUIntArray(message);

            var encryptedList = new List<Tuple<uint, uint>>();

            /* Реализация IGE цепочки */

            var encryptUpperLeft = _syncVectorLeft;
            var encryptUpperRight = _syncVectorRight;

            var encryptBottomLeft = _syncVectorLeft;
            var encryptBottomRight = _syncVectorRight;

            for (int i = 0; i < compactedArray.Count; i += 2)
            {
                var blockLeftPrev = compactedArray[i] ^ encryptUpperLeft;
                var blockRightPrev = compactedArray[i + 1] ^ encryptUpperRight;

                var encryptedBlocks = _be.Encrypt(blockLeftPrev, blockRightPrev);

                var blockLeftAfter = encryptedBlocks.Item1 ^ encryptBottomLeft;
                var blockRightAfter = encryptedBlocks.Item2 ^ encryptBottomRight;

                encryptedList.Add(new Tuple<uint, uint>(blockLeftAfter, blockRightAfter));

                encryptUpperLeft = blockLeftAfter;
                encryptUpperRight = blockRightAfter;

                encryptBottomLeft = compactedArray[i];
                encryptBottomRight = compactedArray[i + 1];
            }
            /* --- */

            return BlockEncryptor.CreateUIntArray(encryptedList);
        }

        public string Decrypt(
            uint[] data)
        {
            var compactedArray = data.ToList();

            if (compactedArray.Count % 2 != 0)
                compactedArray.Add(0);

            var decryptedList = new List<Tuple<uint, uint>>();

            /* Реализация IGE цепочки */

            var encryptUpperLeft = _syncVectorLeft;
            var encryptUpperRight = _syncVectorRight;

            var encryptBottomLeft = _syncVectorLeft;
            var encryptBottomRight = _syncVectorRight;

            for (int i = 0; i < compactedArray.Count; i += 2)
            {
                var blockLeftPrev = compactedArray[i] ^ encryptUpperLeft;
                var blockRightPrev = compactedArray[i + 1] ^ encryptUpperRight;

                var encryptedBlocks = _be.Decrypt(blockLeftPrev, blockRightPrev);

                var blockLeftAfter = encryptedBlocks.Item1 ^ encryptBottomLeft;
                var blockRightAfter = encryptedBlocks.Item2 ^ encryptBottomRight;

                decryptedList.Add(new Tuple<uint, uint>(blockLeftAfter, blockRightAfter));

                encryptUpperLeft = blockLeftAfter;
                encryptUpperRight = blockRightAfter;

                encryptBottomLeft = compactedArray[i];
                encryptBottomRight = compactedArray[i + 1];
            }
            /* --- */

            return BlockEncryptor.DecryptedListToString(decryptedList);
        }

    }
}
