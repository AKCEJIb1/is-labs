﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InformationSecurity.Labs.Framework.CryptSchemes
{
    public class ECB : ICryptScheme
    {
        private BlockEncryptor _be;

        public ECB(BlockEncryptor be = null)
        {
            _be = be ?? new BlockEncryptor(12, (uint)new Random().Next());
        }

        public uint[] Encrypt(
            string message)
        {
            var compactedArray = BlockEncryptor.CompressStringToUIntArray(message);

            var encryptedList = new List<Tuple<uint, uint>>();

            /* Реализация ECB цепочки */

            for (int i = 0; i < compactedArray.Count; i += 2)
                encryptedList.Add(_be.Encrypt(compactedArray[i], compactedArray[i + 1]));
            /* --- */

            return BlockEncryptor.CreateUIntArray(encryptedList);
        }

        public string Decrypt(
            uint[] data)
        {
            var compactedArray = data.ToList();

            if (compactedArray.Count % 2 != 0)
                compactedArray.Add(0);

            var decryptedList = new List<Tuple<uint, uint>>();
            /* Реализация ECB цепочки */

            for (int i = 0; i < compactedArray.Count; i += 2)
                decryptedList.Add(_be.Decrypt(compactedArray[i], compactedArray[i + 1]));
            /* --- */

            return BlockEncryptor.DecryptedListToString(decryptedList);
        }
      
    }
}
