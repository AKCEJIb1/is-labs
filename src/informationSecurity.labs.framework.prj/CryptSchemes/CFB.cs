﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InformationSecurity.Labs.Framework.CryptSchemes
{
    public class CFB : ICryptScheme
    {
        private BlockEncryptor _be;
        private uint _syncVectorLeft, _syncVectorRight;

        public CFB(
            BlockEncryptor be = null,
            uint? syncVectorLeft = null,
            uint? syncVectorRight = null)
        {
            var rand = new Random();

            _be = be ?? new BlockEncryptor(12, (uint)rand.Next());
            _syncVectorLeft = syncVectorLeft ?? (uint)rand.Next();
            _syncVectorRight = syncVectorRight ?? (uint)rand.Next();
        }

        public uint[] Encrypt(
            string message)
        {
            var compactedArray = BlockEncryptor.CompressStringToUIntArray(message);

            var encryptedList = new List<Tuple<uint, uint>>();

            /* Реализация CFB цепочки */

            var firstEncrypt = _be.Encrypt(_syncVectorLeft, _syncVectorRight);

            var blockLeft = firstEncrypt.Item1;
            var blockRight = firstEncrypt.Item2;

            for (int i = 0; i < compactedArray.Count; i += 2)
            {

                var encryptLeft = compactedArray[i] ^ blockLeft;
                var encryptRight = compactedArray[i + 1] ^ blockRight;

                encryptedList.Add(new Tuple<uint, uint>(
                    encryptLeft,
                    encryptRight));

                var encrypt = _be.Encrypt(encryptLeft, encryptRight);

                blockLeft = encrypt.Item1;
                blockRight = encrypt.Item2;
            }

            /*-----*/

            return BlockEncryptor.CreateUIntArray(encryptedList);
        }

        public string Decrypt(
            uint[] data)
        {
            var compactedArray = data.ToList();

            if (compactedArray.Count % 2 != 0)
                compactedArray.Add(0);

            var decryptedList = new List<Tuple<uint, uint>>();

            /* Реализация CFB цепочки */

            // Тут Encrypt - это не ошибка!
            var firstEncrypt = _be.Encrypt(_syncVectorLeft, _syncVectorRight);

            var blockLeft = firstEncrypt.Item1;
            var blockRight = firstEncrypt.Item2;

            for (int i = 0; i < compactedArray.Count; i += 2)
            {
                var encryptLeft = compactedArray[i] ^ blockLeft;
                var encryptRight = compactedArray[i + 1] ^ blockRight;

                decryptedList.Add(new Tuple<uint, uint>(
                    encryptLeft,
                    encryptRight));

                var encrypt = _be.Encrypt(compactedArray[i], compactedArray[i + 1]);
                blockLeft = encrypt.Item1;
                blockRight = encrypt.Item2;
            }
            /* ---- */

            return BlockEncryptor.DecryptedListToString(decryptedList);
        }
    }
}
