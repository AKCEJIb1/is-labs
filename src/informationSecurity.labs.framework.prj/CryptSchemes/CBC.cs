﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace InformationSecurity.Labs.Framework.CryptSchemes
{
    public class CBC : ICryptScheme
    {
        private BlockEncryptor _be;
        private uint _syncVectorLeft, _syncVectorRight;

        public CBC(
            BlockEncryptor be = null,
            uint? syncVectorLeft = null,
            uint? syncVectorRight = null)
        {
            var rand = new Random();

            _be = be ?? new BlockEncryptor(12, (uint)rand.Next());
            _syncVectorLeft = syncVectorLeft ?? (uint)rand.Next();
            _syncVectorRight = syncVectorRight ?? (uint)rand.Next();
        }

        public uint[] Encrypt(
            string message)
        {
            var compactedArray = BlockEncryptor.CompressStringToUIntArray(message);

            var encryptedList = new List<Tuple<uint, uint>>();

            /* Реализация CBC цепочки */

            var blockLeft = compactedArray[0] ^ _syncVectorLeft;
            var blockRight = compactedArray[1] ^ _syncVectorRight;

            for (int i = 0; i < compactedArray.Count; i += 2)
            {
                var encryptedBlocks = _be.Encrypt(blockLeft, blockRight);

                encryptedList.Add(encryptedBlocks);

                if (compactedArray.Count > i + 2)
                {
                    blockLeft = compactedArray[i + 2] ^ encryptedBlocks.Item1;
                    blockRight = compactedArray[i + 2 + 1] ^ encryptedBlocks.Item2;
                }
            }
            /* --- */


            return BlockEncryptor.CreateUIntArray(encryptedList);
        }

        public string Decrypt(
            uint[] data)
        {
            var compactedArray = data.ToList();

            if (compactedArray.Count % 2 != 0)
                compactedArray.Add(0);

            var decryptedList = new List<Tuple<uint, uint>>();

            /* Реализация CBC цепочки */

            var leftVect = _syncVectorLeft;
            var rightVect = _syncVectorRight;

            for (int i = 0; i < compactedArray.Count; i += 2)
            {
                var decryptedBlocks = _be.Decrypt(compactedArray[i], compactedArray[i+1]);

                decryptedList.Add(new Tuple<uint, uint>(
                    decryptedBlocks.Item1 ^ leftVect,
                    decryptedBlocks.Item2 ^ rightVect));

                leftVect = compactedArray[i];
                rightVect = compactedArray[i + 1];
            }
            /* --- */

            return BlockEncryptor.DecryptedListToString(decryptedList);
        }

    }
}
