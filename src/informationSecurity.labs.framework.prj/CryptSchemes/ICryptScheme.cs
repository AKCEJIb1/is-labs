﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InformationSecurity.Labs.Framework.CryptSchemes
{
    public interface ICryptScheme
    {
        /// <summary>
        /// Зашифровать сообщение.
        /// </summary>
        /// <param name="message">Сообщение для шифровки.</param>
        /// <returns>Массив <see cref="uint"/> содержащий в себе зашифрованое сообщение 
        /// (<see cref="uint"/> т.к. размер половины блока данных составляет 8 <see cref="byte"/>).</returns>
        uint[] Encrypt(string message);

        /// <summary>
        /// Расшифровать зашифрованое сообщение.
        /// </summary>
        /// <param name="data">Массив содержащий в себе зашифрованное ранее сообщение.</param>
        /// <returns>Расшифрованное сообщение.</returns>
        string Decrypt(uint[] data);
    }
}
