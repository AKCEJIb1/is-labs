﻿using InformationSecurity.Labs.Framework;
using InformationSecurity.Labs.Framework.CryptSchemes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InformationSecurity.Labs.ConsoleTest
{
    public static class Demos
    {
        /// <summary>
        /// Демонстрация работы второй лабораторной работы.
        /// </summary>
        public static void HashSum()
        {
            var ch = new CryptoHasher();

            Console.Write("Введите сообщения для получения хеш-суммы: ");
            var msg = Console.ReadLine();

            Console.WriteLine($"Вычисляем хеш-сумму для сообщения '{msg}'");

            var hashSum = ch.GenerateHash(msg);

            Console.WriteLine($"Хеш-сумма ({hashSum.Length * 8}-бит): ");
            foreach (var row in hashSum)
            {
                Console.Write($"{row:X}".PadLeft(2, '0') + "");
            }

            Console.WriteLine();
            Console.WriteLine($"Сумма 24 байт хеш-суммы: {hashSum.Sum(x => x)}");
        }
        /// <summary>
        /// Демонстрация работы третьей лабораторной работы.
        /// </summary>
        public static void Normalize()
        {
            var entropyArray = new ulong[12];
            ConsoleKeyInfo prevKey = new ConsoleKeyInfo();
            for (int i = 0; i < 12; i++)
            {
                var ts1 = Rdtsc.Timestamp();

                Console.WriteLine($"Нажмите любую клавишу, осталось {12 - i} из 12 нажатий");
                var key = Console.ReadKey(true);

                if(key.Key == prevKey.Key)
                {
                    Console.BackgroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("Распознано нажатие одинаковых клавиш, повторите попытку!");
                    Console.ResetColor();

                    i--;
                    continue;
                }
                    
                prevKey = key;
                var endTime = Rdtsc.Timestamp() - ts1;
                entropyArray[i] = endTime;
            }

            Console.WriteLine("Получившийся массив:");
            foreach (var item in entropyArray)
            {
                Console.WriteLine($"{item} (0x{item:X})");
            }


            var rsn = new RandomStreamNormalizer();
            var answerRsn = rsn.GenerateKey(entropyArray);

            var ch = new CryptoHasher();
            var answerCh = ch.GenerateHash(entropyArray);

            Console.WriteLine($"Нормализация с помощью сжатия" +
                $" ({answerRsn.Length * 8}-битный ключ):");
            foreach (var row in answerRsn)
            {
                Console.Write($"{row:X}".PadLeft(2, '0') + " ");
            }
            Console.WriteLine();

            Console.WriteLine($"Нормализация с помощью хеш-функции" +
                $" ({answerCh.Length * 8}-битный ключ):");
            foreach (var row in answerCh)
            {
                Console.Write($"{row:X}".PadLeft(2, '0') + " ");
            }
            Console.WriteLine();
        }

        /// <summary>
        /// Демонстрация четвёртой лабораторной работы.
        /// </summary>
        public static void Chains()
        {
            string testMsg = "Я тестовое сообщение, посмотрите на меня!";
            var be = new BlockEncryptor(12, 0x1337);

            uint syncVecLeft = 0xABCDEF01;
            uint syncVecRight = 0x01FEDBCA;

            Console.WriteLine("Проверка ECB:");
            RunCryptScheme(new ECB(be), testMsg);

            Console.WriteLine("\nПроверка CBC:");
            RunCryptScheme(new CBC(be, syncVecLeft, syncVecRight), testMsg);

            Console.WriteLine("\nПроверка CFB:");
            RunCryptScheme(new CFB(be, syncVecLeft, syncVecRight), testMsg);

            Console.WriteLine("\nПроверка IGE:");
            RunCryptScheme(new IGE(be, syncVecLeft, syncVecRight), testMsg);
        }

        /// <summary>
        /// Метод запускающий любую переданную схему шифования для ввода информации пользователем.
        /// </summary>
        /// <param name="cryptScheme">Схема шифрования.</param>
        /// <param name="message">Сообщение для шифрования, 
        /// если передан null, то будет предложено ввести сообщение пользователю.</param>
        public static void RunCryptScheme(ICryptScheme cryptScheme, string message = null)
        {
            if(message == null)
                Console.Write("Введите сообщение для шифрования текста: ");

            var msg = message ?? Console.ReadLine();

            Console.WriteLine($"Оргинальное сообщение [{msg.Length} байт]: {msg}");
            var result = cryptScheme.Encrypt(msg);

            Console.Write($"Зашифрованное сообщение [{4 * result.Length} байт]: ");
            foreach (var row in result)
            {
                Console.Write($"{row:X}".PadLeft(8, '0') + " ");
            }
            Console.WriteLine();
            var decryptResult = cryptScheme.Decrypt(result);
            Console.WriteLine($"Расшифрованное сообщение: {decryptResult}");
        }
    }
}
