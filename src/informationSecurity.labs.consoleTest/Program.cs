﻿using InformationSecurity.Labs.Framework;
using InformationSecurity.Labs.Framework.CryptSchemes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InformationSecurity.Labs.ConsoleTest
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                try
                {
                    ChooseLab();
                    Console.WriteLine();
                }
                catch
                {
                    break;
                }
            }

            Console.WriteLine("Нажмите любую клавишу, чтобы продолжить...");
            Console.ReadKey(true);
        }

        private static void ChooseLab()
        {
            Console.Write("Введите номер лабораторной работы для демонстрации" +
                "\n(0 - для выхода, -1 - очистить экран): ");

            if (int.TryParse(Console.ReadLine(), out var labNum))
            {
                Console.WriteLine();
                switch (labNum)
                {
                    case -1:
                        Console.Clear();
                        break;
                    case 0:
                        throw new Exception();
                    case 1:
                        Demos.RunCryptScheme(new ECB());
                        break;
                    case 2:
                        Demos.HashSum();
                        break;
                    case 3:
                        Demos.Normalize();
                        break;
                    case 4:
                        Demos.Chains();
                        break;
                    default:
                        Console.WriteLine("Нет такой лабораторной работы!");
                        break;
                }

            }
            else
                Console.WriteLine("Пожалуйста, введите число!");
        }
    }
}
